# st - simple terminal

[st][suckless-st] is a simple terminal emulator for X which sucks less.

## Branches

- `source`:
   - Files are from suckless except `.gitignore` and `README.md`.
   - The _source_ branch should **not be patched or customized**.
- `patch/RELEASE/feature`:
   - A _patch_ branch should be created from the _source_ branch.
   - Each feature should be patched on an **individual branch**.
   - The _RELEASE_ should correspond to the st version.
- `platform/RELEASE/os`:
   - A _platform_ branch should be created from the _source_ branch.
   - The _platform_ branch should **not be patched**.
   - The _RELEASE_ should correspond to the st version.
- `build/RELEASE/os`:
   - A _build_ branch should be created from the _source_ branch.
   - Merge the required feature or platform branches into a _build_ branch.
   - Customization should be **after merging branches**.
   - The _RELEASE_ should correspond to the st version.

## Requirements

In order to build st you need the Xlib header files.

## Installation

Edit `config.mk` to match your local setup (st is installed into
the `/usr/local` namespace by default).

Afterwards enter the following command to build and install st (if
necessary as root):

```shell
$ make clean install
```

## Running st

If you did not install st with `make clean install`, you must compile
the st terminfo entry with the following command:

```shell
$ tic -sx st.info
```

See the man page for additional details.

## Keyboard shortcuts

The `MODKEY` is set to `Alt`. The `TERMMOD` is set to `Ctrl+Shift`.

| Function                                          | Key binding           |
|:--------------------------------------------------|:----------------------|
| Increase font size                                | _TERMMOD_ + Page Up   |
| Decrease font size                                | _TERMMOD_ + Page Down |
| Reset to default font size                        | _TERMMOD_ + Home      |
| Copy the selected text to the clipboard selection | _TERMMOD_ + c         |
| Paste from the clipboard selection                | _TERMMOD_ + v         |
| Paste from primary selection                      | _TERMMOD_ + y         |
| Paste from primary selection                      | Shift + Insert        |

## Mouse shortcuts

| Action       | Function                     |
|:-------------|:-----------------------------|
| Middle-click | Paste from primary selection |

## Credits

- [Suckless project][suckless-website]

[suckless-st]: https://st.suckless.org/
[suckless-website]: https://suckless.org/
